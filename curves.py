import numpy as np
import math


class Curve:
    def __call__(self, u):
        raise NotImplementedError()

    def derivative(self, u):
        raise NotImplementedError()

    def second_derivative(self, u):
        raise NotImplementedError()


class Line1D(Curve):
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __call__(self, u):
        v = 1 - u
        return (self.a * v + self.b * u)[:, np.newaxis]

    def derivative(self, u):
        return u[:, np.newaxis]*0.0 + (self.b - self.a)

    def second_derivative(self, u):
        return u[:, np.newaxis]*0.0


class Line2D(Curve):
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __call__(self, u):
        v = 1 - u
        n = len(u)
        p = np.zeros((n, 2))
        p[:, 0] = (self.a[0] * v + self.b[0] * u)
        p[:, 1] = (self.a[1] * v + self.b[1] * u)
        return p

    def derivative(self, u):
        n = len(u)
        p = np.zeros((n, 2))
        p[:, 0] = (self.b[0] - self.a[0])
        p[:, 1] = (self.b[1] - self.a[1])
        return p

    def second_derivative(self, u):
        n = len(u)
        p = np.zeros((n, 2))
        return p


class Circle2D(Curve):
    def __init__(self, r):
        self.r = r

    def __call__(self, u):
        n = len(u)
        p = np.zeros((n, 2))
        p[:, 0] = np.cos(2*math.pi*u)
        p[:, 1] = np.sin(2*math.pi*u)
        return p

    def derivative(self, u):
        n = len(u)
        p = np.zeros((n, 2))
        p[:, 0] = -2*math.pi*np.sin(2*math.pi*u)
        p[:, 1] = 2*math.pi*np.cos(2*math.pi*u)
        return p

    def second_derivative(self, u):
        n = len(u)
        p = np.zeros((n, 2))
        p[:, 0] = -4*math.pi*math.pi*np.cos(2*math.pi*u)
        p[:, 1] = -4*math.pi*math.pi*np.sin(2*math.pi*u)
        return p


class CubicBezier(Curve):
    def __init__(self, A, B, C, D):
        self.n_dims = len(A)
        self.A = A
        self.B = B
        self.C = C
        self.D = D

    def __call__(self, u):
        v = 1-u
        n = len(u)
        p = np.zeros((n, self.n_dims))
        for i in range(self.n_dims):
            p[:, i] += (v*v*v)*self.A[i]
            p[:, i] += 3*v*v*u*self.B[i]
            p[:, i] += 3*v*u*u*self.C[i]
            p[:, i] += u*u*u*self.D[i]
        return p

    def derivative(self, u):
        v = 1-u
        n = len(u)
        p = np.zeros((n, self.n_dims))
        for i in range(self.n_dims):
            p[:, i] += 3*v*v*(self.B[i]-self.A[i])
            p[:, i] += 6*v*u*(self.C[i]-self.B[i])
            p[:, i] += 3*u*u*(self.D[i]-self.C[i])
        return p

    def second_derivative(self, u):
        v = 1 - u
        n = len(u)
        p = np.zeros((n, self.n_dims))
        for i in range(self.n_dims):
            p[:, i] += 6*v*(self.C[i]-2*self.B[i]+self.A[i])
            p[:, i] += 6*u*(self.D[i]-2*self.C[i]+self.B[i])
        return p
