import numpy as np
import matplotlib.pyplot as plt
import curves


# def a_mp_func(v, v1=0.2, a0=7.0, ap=5.0, am=7.0):
#     n = len(v)
#     a_minus = np.zeros((n,))
#     a_plus = np.zeros((n,))
#     alpha1 = np.minimum(v[v > 0]/v1, 1)
#     alpha2 = np.minimum(-v[v <= 0]/v1, 1)
#     a_minus[v > 0] = -(alpha1 * am + (1-alpha1) * a0)
#     a_plus[v > 0] = alpha1 * ap + (1-alpha1) * a0
#     a_minus[v <= 0] = -(alpha2 * ap + (1-alpha2) * a0)
#     a_plus[v <= 0] = alpha2 * am + (1-alpha2) * a0
#     return a_minus, a_plus


def a_mp_func(v, ap=5.0):
    # basic, constant acceleration constraint
    n = len(v)
    a_minus = np.zeros((n,))
    a_plus = np.zeros((n,))
    a_plus[:] = ap
    a_minus[:] = -ap
    return a_minus, a_plus


def constraints_a(p_prime, p_prime2, u, u_dot, jerk_max=50.0, show=False):
    n, n_dims = p_prime.shape

    a = np.zeros((n-1, n_dims))

    a_minus = np.zeros((n-1, n_dims))
    a_plus = np.zeros((n-1, n_dims))

    u_mid = 0.5 * (u[:-1] + u[1:])
    u_dot_mid = 0.5 * (u_dot[:-1] + u_dot[1:])
    p_prime_mid = 0.5 * (p_prime[:-1, :] + p_prime[1:, :])
    p_prime2_mid = 0.5 * (p_prime2[:-1, :] + p_prime2[1:, :])
    u_dot_prime = (u_dot[1:] - u_dot[:-1]) / (u[1:] - u[:-1])
    dt = (u[1:] - u[:-1]) / u_dot_mid
    dt_a = (dt[:-1] + dt[1:]) * 0.5

    u_dot2_min_all = -np.inf * np.ones((n - 1, n_dims))
    u_dot2_max_all = np.inf * np.ones((n - 1, n_dims))

    v = np.zeros((n-1, n_dims))
    for i in range(n_dims):
        v[:, i] = p_prime_mid[:, i] * u_dot_mid
        a[:, i] += p_prime2_mid[:, i] * u_dot_mid * u_dot_mid
        a[:, i] += p_prime_mid[:, i] * u_dot_mid * u_dot_prime

        a_plus_next = a[:-1, i] + jerk_max * dt_a
        a_minus_next = a[:-1, i] - jerk_max * dt_a
        a_plus_prev = a[1:, i] + jerk_max * dt_a
        a_minus_prev = a[1:, i] - jerk_max * dt_a

        # boundary condition
        # a_minus[0, i] = a_minus_prev[0]
        # a_minus[-1, i] = a_minus_next[-1]
        # a_plus[0, i] = a_plus_prev[0]
        # a_plus[-1, i] = a_plus_next[-1]

        # zero acceleration beyond curve
        a_minus[0] = -jerk_max * dt_a[0]
        a_plus[0] = jerk_max * dt_a[0]
        a_minus[-1] = -jerk_max * dt_a[-1]
        a_plus[-1] = jerk_max * dt_a[-1]

        a_minus[1:-1, i] = np.maximum(a_minus_next[:-1], a_minus_prev[1:])
        a_plus[1:-1, i] = np.minimum(a_plus_next[:-1], a_plus_prev[1:])

        # min and max prescribed by speed
        a_neg_bound, a_pos_bound = a_mp_func(v[:, i])
        a_minus[:, i] = np.minimum(a_minus[:, i], a_pos_bound)
        a_minus[:, i] = np.maximum(a_minus[:, i], a_neg_bound)
        a_plus[:, i] = np.minimum(a_plus[:, i], a_pos_bound)
        a_plus[:, i] = np.maximum(a_plus[:, i], a_neg_bound)

        # acceleration along the curve only
        a_plus_along = a_plus[:, i] - p_prime2_mid[:, i] * u_dot_mid*u_dot_mid
        a_minus_along = a_minus[:, i] - p_prime2_mid[:, i] * u_dot_mid*u_dot_mid

        mask = np.abs(p_prime_mid[:, i]) > 1e-7

        u_dot2_1 = a_plus_along[mask]/p_prime_mid[mask, i]
        u_dot2_2 = a_minus_along[mask]/p_prime_mid[mask, i]
        u_dot2_min_all[mask, i] = np.minimum(u_dot2_1, u_dot2_2)
        u_dot2_max_all[mask, i] = np.maximum(u_dot2_1, u_dot2_2)

    u_dot2_min = np.max(u_dot2_min_all, axis=-1)
    u_dot2_max = np.min(u_dot2_max_all, axis=-1)

    if show:
        plt.figure()
        plt.plot(u_mid, u_dot_prime * u_dot_mid, 'k')
        plt.plot(u_mid, u_dot2_min, 'b')
        plt.plot(u_mid, u_dot2_max, 'r')
        plt.title("u_dot2")

        plt.figure()
        for i in range(n_dims):
            plt.subplot(n_dims, 1, i+1)
            plt.plot(u_mid, u_dot2_min_all[:, i], 'b')
            plt.plot(u_mid, u_dot2_max_all[:, i], 'r')
            plt.title(f"u_dot2_{i}")

        plt.figure()
        for i in range(n_dims):
            plt.subplot(n_dims, 1, i+1)
            plt.plot(u_mid, a[:, i], 'k')
            plt.plot(u_mid, a_minus[:, i], 'b')
            plt.plot(u_mid, a_plus[:, i], 'r')
            plt.title(f"a_{i}")

    return u_dot2_min, u_dot2_max


def constraints_v(p_prime, u, u_dot, u_dot2_min, u_dot2_max, v_abs_max=4.0, show=False):
    n, n_dims = p_prime.shape
    u_dot_min = np.zeros((n,))
    u_dot_max = np.zeros((n,))

    # v = np.zeros((n - 1, n_dims))
    du = u[1:] - u[:-1]
    u_dot_max_next = np.sqrt(np.maximum(u_dot[:-1]**2 + 2 * u_dot2_max * du, 0))
    u_dot_min_next = np.sqrt(np.maximum(u_dot[:-1]**2 + 2 * u_dot2_min * du, 0))
    u_dot_min_prev = np.sqrt(np.maximum(u_dot[1:]**2 - 2 * u_dot2_max * du, 0))
    u_dot_max_prev = np.sqrt(np.maximum(u_dot[1:]**2 - 2 * u_dot2_min * du, 0))
    u_dot_min[0] = u_dot_min_prev[0]
    u_dot_min[-1] = u_dot_min_next[-1]
    u_dot_max[0] = u_dot_max_prev[0]
    u_dot_max[-1] = u_dot_max_next[-1]
    u_dot_min[1:-1] = np.maximum(u_dot_min_next[:-1], u_dot_min_prev[1:])
    u_dot_max[1:-1] = np.minimum(u_dot_max_next[:-1], u_dot_max_prev[1:])
    u_dot_min = np.minimum(u_dot_min, u_dot_max)

    p_prime_norm = np.linalg.norm(p_prime, axis=-1)
    u_dot_min[:] = np.maximum(u_dot_min, 0)
    u_dot_max[:] = np.minimum(u_dot_max, v_abs_max / p_prime_norm)

    if show:
        plt.figure()
        plt.plot(u, u_dot, 'k')
        plt.plot(u, u_dot_min, 'b')
        plt.plot(u, u_dot_max, 'r')
        plt.title("u_dot")

    return u_dot_min, u_dot_max


def main():
    n = 256
    # c = curves.Line1D(0, 4)
    # c = curves.Line2D([0, 0], [4, 0])
    # c = curves.CubicBezier([0, 0], [1, 0], [1, 2], [0, 0.7])
    c = curves.CubicBezier([0, 0], [1, 0], [1, 1], [2, 1])
    # c = curves.Circle2D(1.5)
    u = np.linspace(0, 1, n)
    u_dot = np.zeros((n,))
    p = c(u)
    p_prime = c.derivative(u)
    p_prime2 = c.second_derivative(u)

    u_dot[1:-1] = 1e-4
    # u_dot[:] = 0.1*(2*u - 2*u*u)

    # lambd = 0.1
    lambd = 0.3
    optimizing = True
    max_itr = int(5e4)
    k = 0
    while optimizing:
        show = k == -1
        u_dot2_min, u_dot2_max = constraints_a(p_prime, p_prime2, u, u_dot, show=show)
        u_dot_min, u_dot_max = constraints_v(p_prime, u, u_dot, u_dot2_min, u_dot2_max, show=show)

        if show:
            plt.show()
            return

        mse_diff = np.mean(np.square(u_dot[1:-1] - u_dot_max[1:-1]))
        u_dot[1:-1] = lambd * u_dot_max[1:-1] + (1 - lambd) * u_dot[1:-1]
        k += 1
        optimizing = (mse_diff > 1e-10) and (k < max_itr)

    np.save("curves/u.npy", u)
    np.save("curves/u_dot.npy", u_dot)
    np.save("curves/p.npy", p)
    np.save("curves/p_prime.npy", p_prime)
    np.save("curves/p_prime2.npy", p_prime2)

    print(f"{k} steps")

    u_mid = (u[:-1] + u[1:]) * 0.5

    plt.figure()
    plt.plot(u, u_dot, 'k')
    plt.plot(u, u_dot_min, 'b')
    plt.plot(u, u_dot_max, 'r')

    plt.figure()
    plt.plot(u_mid, u_dot2_min, 'b')
    plt.plot(u_mid, u_dot2_max, 'r')

    plt.show()


if __name__ == "__main__":
    main()
