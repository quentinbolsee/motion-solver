import matplotlib.pyplot as plt
import numpy as np
import math


def hermite1000(x, h):
    mask = (x >= -h) & (x <= h)
    return mask * np.polyval([2/(h*h*h), -3/(h*h), 0, 1], np.abs(x))


def hermite0100(x, h):
    mask = (x >= -h) & (x <= h)
    return mask * np.polyval([1/(h*h), -2/h, 1, 0], np.abs(x))*np.sign(x)


def main():
    n = 512
    n_coarse = 8
    x = np.linspace(-2, 10, n)
    y = np.zeros((n,))
    x_coarse = np.arange(n_coarse)
    y_coarse = np.sin(math.pi * x_coarse/n_coarse)

    y_coarse_pad = np.zeros((n_coarse+2,))
    y_coarse_pad[1:-1] = y_coarse

    # y_dot = y_coarse_pad[2:] - y_coarse_pad[:-2]
    y_dot = math.pi * np.cos(math.pi * x_coarse/n_coarse) / n_coarse
    # y_dot = np.cos(math.pi * x_coarse/n_coarse)
    # y_coarse = [1, 2, 0.5, 3, ]

    for i in range(n_coarse):
        h1 = hermite1000(x - i, 1)
        h2 = hermite0100(x - i, 1)
        y += h1 * y_coarse[i]
        y += h2 * y_dot[i]

    plt.figure()
    plt.plot(x_coarse, y_coarse, "r.-")
    # plt.plot(x_coarse, y_dot, "r.-")
    plt.plot(x, y, 'b')
    plt.show()

    return

    # for
    h1 = hermite1000(x, 1)
    h2 = hermite0100(x, 1)
    h1_1 = hermite1000(x-1, 1)
    h2_1 = hermite0100(x-1, 1)

    #   1    2
    y = h1 + 2*h1_1 + h2 * 1 + h2_1 * 0

    plt.figure()
    plt.plot(x, y, "b")
    # plt.plot(x, h2, "r")
    # plt.plot(x, h1_1, "b")
    # plt.plot(x, h2_1, "r")
    plt.show()


def main2():
    x = np.loadtxt("x.txt")
    v = np.loadtxt("v.txt")
    t = np.loadtxt("t.txt")
    # p = np.polyfit(t, x, 5)
    # x_approx = np.polyval(p,t)

    k = 60
    t_coarse = t[::k]
    x_coarse = x[::k]
    v_coarse = v[::k]
    # print(len(t))
    # t_coa
    # x_coarse = x[]

    # x_dot = np.zeros_like(x)
    # x_dot[:-1] = (x[1:] - x[:-1])/(t[1:] - t[:-1])

    plt.figure()
    plt.plot(t, x, 'b')
    plt.plot(t_coarse, x_coarse, 'bo')
    # plt.plot(t, x_dot)
    # plt.plot(t, v, 'r')
    #
    # plt.show()
    # return

    h = t_coarse[1] - t_coarse[0]

    n_coarse = len(t_coarse)

    x_approx = np.zeros_like(x)
    for i in range(n_coarse):
        s1 = hermite1000(t - i * h, h)
        s2 = hermite0100(t - i * h, h)
        x_approx += s1 * x_coarse[i]
        x_approx += s2 * v_coarse[i]

    print(n_coarse)

    plt.plot(t, x_approx, 'r')

    plt.figure()
    plt.plot(t, x-x_approx, "r")
    plt.title("error")
    # plt.plot(t_coarse, x_coarse, "r.")
    plt.show()
    # print(x)


if __name__ == "__main__":
    main2()
