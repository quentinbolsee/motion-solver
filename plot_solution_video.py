import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import time

ani = None
interval_ms = 16
t = 0


def update(frame, lines, u, u_dot, p, p_prime, p_prime2):
    global ani, interval_ms, t

    n = len(u)
    u_dot_mid = (u_dot[:-1] + u_dot[1:])*0.5
    t_delta = (u[1:] - u[:-1])/u_dot_mid
    t_delta_ext = np.zeros((n,))
    t_delta_ext[1:] = t_delta
    t_axis = np.cumsum(t_delta_ext)
    a = np.zeros((n-1, 2))
    p_prime_mid = 0.5 * (p_prime[:-1, :] + p_prime[1:, :])
    p_prime2_mid = 0.5 * (p_prime2[:-1, :] + p_prime2[1:, :])
    u_dot_prime = (u_dot[1:] - u_dot[:-1]) / (u[1:] - u[:-1])
    for i in range(2):
        a[:, i] += p_prime2_mid[:, i] * u_dot_mid * u_dot_mid
        a[:, i] += p_prime_mid[:, i] * u_dot_mid * u_dot_prime

    if t > t_axis[-1]:
        # cycle
        t = 0
        # time.sleep(0.5)

    p_now = np.zeros((2,))
    a_now = np.zeros((2,))
    p_now[0] = np.interp(t, t_axis, p[:, 0])
    p_now[1] = np.interp(t, t_axis, p[:, 1])
    a_now[0] = np.interp(t, t_axis[:-1], a[:, 0])
    a_now[1] = np.interp(t, t_axis[:-1], a[:, 1])

    a_scale = 0.03

    lines[0].set_xdata(p[:, 0])
    lines[0].set_ydata(p[:, 1])
    lines[1].set_xdata(p_now[0])
    lines[1].set_ydata(p_now[1])
    lines[2].set_xdata([p_now[0], p_now[0]+a_scale*a_now[0]])
    lines[2].set_ydata([p_now[1], p_now[1]+a_scale*a_now[1]])

    lines[4].set_xdata(t_axis)
    lines[4].set_ydata(u_dot)
    lines[5].set_xdata([t, t])
    lines[5].set_ydata([0, np.max(u_dot)*1.5])

    t += interval_ms/1000.0

    return lines


def main_func():
    global ani, interval_ms

    u = np.load("curves/u.npy")
    u_dot = np.load("curves/u_dot.npy")
    p = np.load("curves/p.npy")
    p_prime = np.load("curves/p_prime.npy")
    p_prime2 = np.load("curves/p_prime2.npy")
    n = len(u)

    u_mid = (u[:-1] + u[1:]) * 0.5
    u_delta = u[1:] - u[:-1]
    u_dot_mid = (u_dot[:-1] + u_dot[1:]) * 0.5

    t_delta = u_delta / u_mid
    a = (u_dot[1:] - u_dot[:-1]) / t_delta

    t_delta_ext = np.zeros((n,))
    t_delta_ext[1:] = t_delta
    t = np.cumsum(t_delta_ext)

    fig, axes = plt.subplots(1, 2)
    ax = axes[0]
    ax2 = axes[1]
    ln0, = ax.plot([], [], 'b')
    ln1, = ax.plot([], [], 'k.', ms=20)
    ln2, = ax.plot([], [], 'r-')
    ln3, = ax.plot([], [], 'g-')
    ax.set_xlim([np.min(p[:, 0])-0.5, np.max(p[:, 0])+0.5])
    ax.set_ylim([np.min(p[:, 1])-0.5, np.max(p[:, 1])+0.5])
    ax.set_aspect("equal")
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_title("Motion")
    # ax.legend(["speed", "min", "max"])

    ln4, = ax2.plot([], [], 'b')
    ln5, = ax2.plot([], [], 'k')
    ln6, = ax2.plot([], [], 'r')
    ax2.set_xlim([0, 2])
    ax2.set_ylim([0, 1])
    ax2.set_xlabel("t")
    ax2.set_ylabel("u_dot")
    ax2.set_title("Curve reading speed")
    # ax2.legend(["acc.", "min", "max"])

    lines = [ln0, ln1, ln2, ln3, ln4, ln5, ln6]
    plt.tight_layout()

    def init():
        return lines

    ani = FuncAnimation(fig, update, init_func=init, fargs=(lines, u, u_dot, p, p_prime, p_prime2), blit=True, interval=interval_ms)
    plt.show()


if __name__ == "__main__":
    main_func()
